import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:panel_pwa/components/data/route_name.dart';
import 'package:panel_pwa/components/di.dart';

void main() async {
  runApp(MaterialApp(
    navigatorKey: materialKey,
    initialRoute: RouteName.splash.name,
    onGenerateRoute: (settings) {
      final RouteName route = RouteName.fromString(settings.name);
      return CupertinoPageRoute(builder: (context) => route.screen);
    },
  ));
}
