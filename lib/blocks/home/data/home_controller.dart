import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/convert/screens/convert_screen.dart';
import 'package:panel_pwa/blocks/rates/screens/rates_screen.dart';
import 'package:panel_pwa/components/data/route_name.dart';
import 'package:panel_pwa/components/di.dart';
import 'package:panel_pwa/components/services/storage_service.dart';

class HomeController extends Cubit<void> {
  HomeController() : super(null);

  final List<Widget> pages = const [RatesScreen(), ConvertScreen()];

  void logOut() async {
    getIt<StorageService>().clearAll();
    materialKey.currentState?.pushReplacementNamed(RouteName.auth.name);
  }
}
