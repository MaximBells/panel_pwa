import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/home/data/home_controller.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/static/app_string.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: BlocProvider(
        create: (context) => HomeController(),
        child: CupertinoTabScaffold(
          tabBar: CupertinoTabBar(
            inactiveColor: Colors.black,
            activeColor: AppColor.blue,
            height: 72,
            iconSize: 24,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.money_dollar_circle),
                    SizedBox(
                      height: 8.5,
                    ),
                    Text(
                      AppString.rates,
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
              BottomNavigationBarItem(
                icon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(CupertinoIcons.arrow_right_arrow_left),
                    SizedBox(
                      height: 8.5,
                    ),
                    Text(
                      AppString.convert,
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
          tabBuilder: (BuildContext context, int index) {
            return CupertinoTabView(
              builder: (BuildContext context) {
                return context.read<HomeController>().pages[index];
              },
            );
          },
        ),
      ),
    );
  }
}
