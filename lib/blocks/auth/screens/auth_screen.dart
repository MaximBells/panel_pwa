import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/auth/data/auth_controller.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/margin.dart';
import 'package:panel_pwa/components/widgets/standard_button.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';
import 'package:panel_pwa/components/widgets/standard_textfield.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthController(),
      child: Builder(builder: (context) {
        return BlocBuilder<AuthController, void>(builder: (
            context,
            state,
            ) {
          return Scaffold(
            body: Margin(
              margin: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const StandardText(
                    text: AppString.welcomeBack,
                    margin: EdgeInsets.only(bottom: 48),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                    ),
                  ),
                  const StandardText(
                    text: AppString.login,
                    margin: EdgeInsets.only(bottom: 8),
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                    ),
                  ),
                  StandardTextField(
                    margin: const EdgeInsets.only(bottom: 16),
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-z]')),
                    ],
                    textEditingController:
                    context.read<AuthController>().loginController,
                  ),
                  const StandardText(
                    text: AppString.password,
                    margin: EdgeInsets.only(bottom: 8),
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                    ),
                  ),
                  StandardTextField(
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[a-z]')),
                    ],
                    margin: const EdgeInsets.only(bottom: 24),
                    textEditingController:
                    context.read<AuthController>().passwordController,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StandardButton(
                        onPressed: () {
                          context.read<AuthController>().auth();
                        },
                        text: AppString.signIn,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        });
      }),
    );
  }
}
