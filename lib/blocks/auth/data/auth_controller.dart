import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/components/data/route_name.dart';
import 'package:panel_pwa/components/di.dart';
import 'package:panel_pwa/components/services/crypt_service.dart';
import 'package:panel_pwa/components/services/snackbar_service.dart';
import 'package:panel_pwa/components/services/storage_service.dart';
import 'package:panel_pwa/components/static/app_string.dart';

class AuthController extends Cubit<void> {
  AuthController() : super(null);

  final TextEditingController loginController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  String get _text => loginController.text + passwordController.text;

  void auth() async {
    if (loginController.text.isEmpty || passwordController.text.isEmpty) {
      SnackBarService.showSnackBar(
        AppString.fieldsEmpty,
        backGroundColor: Colors.red,
      );
      return;
    }

    if (getIt<CryptService>().checkPassword(_text)) {
      SnackBarService.showSnackBar(
        AppString.success,
        backGroundColor: Colors.green,
      );
      await Future.delayed(const Duration(seconds: 1));
      getIt<StorageService>().writeValue(
          StoreKey.authAttempt, DateTime.now().millisecondsSinceEpoch);
      materialKey.currentState?.pushReplacementNamed(RouteName.home.name);
    } else {
      SnackBarService.showSnackBar(
        AppString.fieldsWrong,
        backGroundColor: Colors.red,
      );
    }
  }
}
