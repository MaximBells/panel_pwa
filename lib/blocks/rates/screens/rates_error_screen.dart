import 'package:flutter/material.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';

class RatesErrorScreen extends StatelessWidget {
  const RatesErrorScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.error_outline_sharp,
            size: 36,
            color: AppColor.blue,
          ),
          SizedBox(
            height: 16,
          ),
          StandardText(
            text: AppString.ratesError,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }
}
