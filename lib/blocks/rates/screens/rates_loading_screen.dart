import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';

class RatesLoadingScreen extends StatelessWidget {
  const RatesLoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const CupertinoPageScaffold(
        child: SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CupertinoActivityIndicator(
            color: AppColor.blue,
            radius: 20,
          ),
          SizedBox(
            height: 16,
          ),
          StandardText(
            text: AppString.ratesLoading,
            style: TextStyle(
              color: AppColor.grey,
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          )
        ],
      ),
    ));
  }
}
