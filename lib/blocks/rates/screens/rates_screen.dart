import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/rates/data/rates_cubit.dart';
import 'package:panel_pwa/blocks/rates/widgets/rates_list_widget.dart';
import 'package:panel_pwa/blocks/rates/screens/rates_error_screen.dart';
import 'package:panel_pwa/blocks/rates/screens/rates_loading_screen.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/log_out_button.dart';

class RatesScreen extends StatelessWidget {
  const RatesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RatesCubit(),
      child: Builder(builder: (context) {
        return BlocBuilder<RatesCubit, RatesState>(builder: (context, state) {
          return CupertinoPageScaffold(
              navigationBar: CupertinoNavigationBar(
                leading: IconButton(
                  onPressed: () {
                    context.read<RatesCubit>().updateRates();
                  },
                  icon: const Icon(Icons.refresh),
                  iconSize: 24,
                ),
                middle: const Text(
                  AppString.rates,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      decoration: TextDecoration.none),
                ),
                trailing: const LogOutButton(),
              ),
              child: Builder(builder: (context) {
                if (state is RatesLoading || state is RatesInitial) {
                  return const RatesLoadingScreen();
                } else if (state is RatesLoaded) {
                  return RatesListWidget(rates: state.rates);
                } else {
                  return const RatesErrorScreen();
                }
              }));
        });
      }),
    );
  }
}
