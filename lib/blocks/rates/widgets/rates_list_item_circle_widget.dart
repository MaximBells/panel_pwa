import 'dart:math';

import 'package:flutter/material.dart';
import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';

class RatesListItemCircleWidget extends StatelessWidget {
  const RatesListItemCircleWidget({Key? key, required this.model})
      : super(key: key);

  final RateModel model;

  Color get _randomColor =>
      Colors.primaries[Random().nextInt(Colors.primaries.length - 1)];

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 2,
      shape: CircleBorder(),
      child: Container(
        width: 24,
        height: 24,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: _randomColor,
        ),
        child: Center(
          child: StandardText(
            text: model.sign,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14,
            ),
          ),
        ),
      ),
    );
  }
}
