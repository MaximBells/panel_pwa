import 'package:flutter/material.dart';
import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/blocks/rates/widgets/rates_list_item_circle_widget.dart';
import 'package:panel_pwa/components/widgets/margin.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';

class RatesListItemWidget extends StatelessWidget {
  const RatesListItemWidget({super.key, required this.model});

  final RateModel model;

  @override
  Widget build(BuildContext context) {
    return Margin(
      margin: const EdgeInsets.symmetric(vertical: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              RatesListItemCircleWidget(model: model),
              const SizedBox(
                width: 12,
              ),
              StandardText(
                text: model.symbol ?? "",
                style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
              )
            ],
          ),
          StandardText(text: '\$${model.price}'),
        ],
      ),
    );
  }
}
