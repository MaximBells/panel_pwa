import 'package:flutter/material.dart';
import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/blocks/rates/widgets/rates_list_item_widget.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/widgets/margin.dart';

class RatesListWidget extends StatelessWidget {
  const RatesListWidget({Key? key, required this.rates}) : super(key: key);
  final List<RateModel> rates;

  @override
  Widget build(BuildContext context) {
    return Margin(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      child: ListView.separated(
        itemBuilder: (context, index) {
          return RatesListItemWidget(model: rates[index]);
        },
        separatorBuilder: (context, index) {
          return const Divider(
            color: AppColor.grey,
          );
        },
        itemCount: rates.length,
      ),
    );
  }
}
