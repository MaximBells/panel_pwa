import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/blocks/rates/data/rates_repository.dart';

abstract class RatesState {}

class RatesInitial extends RatesState {}

class RatesLoading extends RatesState {}

class RatesError extends RatesState {}

class RatesLoaded extends RatesState {
  final List<RateModel> rates;

  RatesLoaded(this.rates);
}

class RatesCubit extends Cubit<RatesState> {
  RatesCubit() : super(RatesInitial()) {
    setTimer();
    updateRates();
  }

  final RatesRepository ratesRepository = RatesRepository();

  int _timerCount = 30;

  void setTimer() {
    Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_timerCount <= 0) {
        _timerCount = 30;
        updateRates();
      } else {
        _timerCount -= 1;
      }
    });
  }

  Future<void> updateRates() async {
    if (state is RatesLoading || isClosed) {
      return;
    }
    _timerCount = 30;
    emit(RatesLoading());
    final List<RateModel> rates = await ratesRepository.getRates();
    if (rates.isEmpty) {
      emit(RatesError());
    } else {
      emit(RatesLoaded(rates));
    }
  }
}
