import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/components/di.dart';
import 'package:panel_pwa/components/services/request_service.dart';
import 'package:panel_pwa/components/static/app_url.dart';

class RatesRepository {
  Future<List<RateModel>> getRates() async {
    final response = await getIt<RequestService>().makeRequest(AppUrl.ratesUrl);
    if (response?['data'] == null) {
      return [];
    }
    List<dynamic> data = response['data'];
    return List<RateModel>.from(data.map((e) => RateModel.fromJson(e)));
  }
}
