import 'package:panel_pwa/components/extensions/double_extension.dart';

enum RateType {
  fiat,
  crypto;

  const RateType();

  factory RateType.fromString(String? value) {
    for (var element in RateType.values) {
      if (element.name == value) {
        return element;
      }
    }
    return RateType.fiat;
  }
}

class RateModel {
  final String? id;
  final String? symbol;
  final String? currencySymbol;

  final RateType? type;
  final double? rateUsd;

  String get sign {
    if (currencySymbol != null) {
      return currencySymbol!.length > 1
          ? currencySymbol!.substring(0, 2)
          : currencySymbol!;
    }
    final String name = symbol ?? "";
    return name.substring(0, 1);
  }

  String get price {
    final priceToShow = rateUsd ?? 0.0;
    return priceToShow.toStringAsFixed(8);
  }

  RateModel({
    this.id,
    this.type,
    this.currencySymbol,
    this.rateUsd,
    this.symbol,
  });

  factory RateModel.fromJson(dynamic json) {
    if (json == null) {
      return RateModel();
    }
    return RateModel(
      id: json['id'],
      type: RateType.fromString(json['type']),
      currencySymbol: json['currencySymbol'],
      rateUsd: double.tryParse(json['rateUsd']?.toString() ?? "")
          ?.toDoubleAsFixed(18),
      symbol: json['symbol'],
    );
  }
}
