import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/components/extensions/double_extension.dart';

class ConvertRepository {
  static String getConvertedTo({
    required List<RateModel> rates,
    required int fromIndex,
    required int toIndex,
  }) {
    double convertedValue =
        ((rates[fromIndex].rateUsd! / rates[toIndex].rateUsd!) * 1.03)
            .minimizeDouble();
    return '$convertedValue ${rates[toIndex].symbol}';
  }

  static String getInfoTo({
    required List<RateModel> rates,
    required int fromIndex,
    required int toIndex,
  }) {
    double convertedValue =
        ((rates[fromIndex].rateUsd! / rates[toIndex].rateUsd!))
            .minimizeDouble();
    return '($convertedValue ${rates[toIndex].symbol} + 3%)';
  }
}
