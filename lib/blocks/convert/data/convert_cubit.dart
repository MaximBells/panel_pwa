import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/rates/data/rate_model.dart';
import 'package:panel_pwa/blocks/rates/data/rates_repository.dart';
import 'package:panel_pwa/components/extensions/double_extension.dart';
import 'package:panel_pwa/components/utils.dart';

enum PickerType {
  from,
  to;
}

abstract class ConvertState {
  final List<RateModel> rates;
  final int fromIndex;
  final int toIndex;

  ConvertState({
    this.toIndex = 1,
    this.fromIndex = 0,
    this.rates = const [],
  });
}

class ConvertLoading extends ConvertState {}

class ConvertInitial extends ConvertState {}

class ConvertError extends ConvertState {}

class ConvertLoaded extends ConvertState {
  ConvertLoaded({
    required super.toIndex,
    required super.fromIndex,
    required super.rates,
  });
}

class ConvertCubit extends Cubit<ConvertState> {
  ConvertCubit() : super(ConvertInitial()) {
    setTimer();
    initConverts();
  }

  final RatesRepository ratesRepository = RatesRepository();

  final TextEditingController fromController = TextEditingController();
  final TextEditingController toController = TextEditingController();

  int _timerCount = 30;

  void setTimer() {
    Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_timerCount <= 0) {
        _timerCount = 30;
        uploadRates();
      } else {
        _timerCount -= 1;
      }
    });
  }

  void uploadRates() async {
    if (state is ConvertLoading || isClosed) {
      return;
    }
    emit(ConvertLoading());
    final List<RateModel> rates = await ratesRepository.getRates();
    if (rates.isNotEmpty) {
      emit(ConvertLoaded(
        toIndex: state.toIndex,
        fromIndex: state.fromIndex,
        rates: rates,
      ));
    } else {
      emit(ConvertError());
    }
  }

  void initConverts() async {
    emit(ConvertLoading());
    final rates = await ratesRepository.getRates();
    if (rates.isNotEmpty) {
      fromController.text = rates[0].symbol ?? "";
      toController.text = rates[1].symbol ?? "";
      emit(ConvertLoaded(
        rates: rates,
        fromIndex: 0,
        toIndex: 1,
      ));
    } else {
      emit(ConvertError());
    }
  }

  void updateConvert({int? fromIndex, int? toIndex}) {
    if (state is ConvertLoaded) {
      emit(ConvertLoaded(
        rates: state.rates,
        fromIndex: fromIndex ?? state.fromIndex,
        toIndex: toIndex ?? state.toIndex,
      ));
      fromController.text = state.rates[state.fromIndex].symbol ?? "";
      toController.text = state.rates[state.toIndex].symbol ?? "";
    }
  }

  void openPicker(
    BuildContext context, {
    required PickerType type,
  }) {
    final List<String> names = List<String>.of(
      state.rates.map((e) => e.symbol ?? ""),
    );

    names.removeAt(
      type == PickerType.from ? state.toIndex : state.fromIndex,
    );
    final int initialTerm = names.indexWhere(
      (element) =>
          element ==
          (state
                  .rates[
                      type == PickerType.from ? state.fromIndex : state.toIndex]
                  .symbol ??
              ""),
    );
    Utils.showDialog(context,
        children: List<Widget>.generate(names.length, (
          int index,
        ) {
          return Center(
            child: Text(
              names[index],
            ),
          );
        }),
        initialTerm: initialTerm, onSelected: (
      int selectedItem,
    ) {
      final selectedIndex = state.rates.indexWhere(
        (element) => element.symbol == names[selectedItem],
      );
      updateConvert(
        fromIndex: type == PickerType.from ? selectedIndex : null,
        toIndex: type == PickerType.to ? selectedIndex : null,
      );
    });
  }
}
