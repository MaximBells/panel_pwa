import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/convert/data/convert_cubit.dart';
import 'package:panel_pwa/blocks/convert/data/convert_repository.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';

class ConvertStatusWidget extends StatelessWidget {
  const ConvertStatusWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: AppColor.lightGrey,
        borderRadius: BorderRadius.circular(4),
      ),
      padding: const EdgeInsets.symmetric(vertical: 24),
      child: BlocBuilder<ConvertCubit, ConvertState>(
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StandardText(
                text: '1 ${state.rates[state.fromIndex].symbol}',
                margin: const EdgeInsets.only(bottom: 16),
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                ),
              ),
              const Icon(
                CupertinoIcons.arrow_right_arrow_left,
                size: 24,
                color: AppColor.darkGrey,
              ),
              StandardText(
                text: ConvertRepository.getConvertedTo(
                  rates: state.rates,
                  fromIndex: state.fromIndex,
                  toIndex: state.toIndex,
                ),
                margin: const EdgeInsets.only(top: 16, bottom: 8),
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 24,
                  color: AppColor.blue,
                ),
              ),
              StandardText(
                text: ConvertRepository.getInfoTo(
                  rates: state.rates,
                  fromIndex: state.fromIndex,
                  toIndex: state.toIndex,
                ),
                margin: const EdgeInsets.only(bottom: 16),
                style: const TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
