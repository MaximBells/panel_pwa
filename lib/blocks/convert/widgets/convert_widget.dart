import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/convert/data/convert_cubit.dart';
import 'package:panel_pwa/blocks/convert/widgets/convert_status_widget.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/standard_text.dart';
import 'package:panel_pwa/components/widgets/standard_textfield.dart';

class ConvertWidget extends StatelessWidget {
  const ConvertWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const StandardText(
            margin: EdgeInsets.only(bottom: 8),
            text: AppString.from,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          StandardTextField(
            onTap: () {
              context.read<ConvertCubit>().openPicker(
                    context,
                    type: PickerType.from,
                  );
            },
            margin: const EdgeInsets.only(bottom: 16),
            textEditingController: context.read<ConvertCubit>().fromController,
            readOnly: true,
            suffix: const Icon(
              Icons.keyboard_arrow_down,
              color: AppColor.grey,
              size: 24,
            ),
          ),
          const StandardText(
            margin: EdgeInsets.only(bottom: 8),
            text: AppString.to,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          StandardTextField(
            onTap: () {
              context.read<ConvertCubit>().openPicker(
                    context,
                    type: PickerType.to,
                  );
            },
            margin: const EdgeInsets.only(bottom: 24),
            textEditingController: context.read<ConvertCubit>().toController,
            readOnly: true,
            suffix: const Icon(
              Icons.keyboard_arrow_down,
              color: AppColor.grey,
              size: 24,
            ),
          ),
          const ConvertStatusWidget(),
        ],
      ),
    );
  }
}
