import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/convert/data/convert_cubit.dart';
import 'package:panel_pwa/blocks/convert/widgets/convert_widget.dart';
import 'package:panel_pwa/blocks/rates/screens/rates_error_screen.dart';
import 'package:panel_pwa/blocks/rates/screens/rates_loading_screen.dart';
import 'package:panel_pwa/components/static/app_string.dart';
import 'package:panel_pwa/components/widgets/log_out_button.dart';

class ConvertScreen extends StatelessWidget {
  const ConvertScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ConvertCubit(),
      child: Builder(builder: (context) {
        return BlocBuilder<ConvertCubit, ConvertState>(
            builder: (context, state) {
          return CupertinoPageScaffold(
              navigationBar: const CupertinoNavigationBar(
                leading: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppString.convert,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                          decoration: TextDecoration.none),
                    ),
                  ],
                ),
                trailing: LogOutButton(),
              ),
              child: SafeArea(
                child: Builder(builder: (context) {
                  if (state is ConvertLoading || state is ConvertInitial) {
                    return const RatesLoadingScreen();
                  } else if (state is ConvertLoaded) {
                    return const ConvertWidget();
                  } else {
                    return const RatesErrorScreen();
                  }
                }),
              ));
        });
      }),
    );
  }
}
