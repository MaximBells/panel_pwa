class AppString {
  static const needToLogIn = 'You need to log into your account.';
  static const welcomeBack = 'Welcome back!';
  static const login = 'Login';
  static const password = 'Password';
  static const signIn = 'Sign in';
  static const fieldsEmpty = 'The username or password field is empty.';
  static const fieldsWrong =
      'The username or password has been entered incorrectly.';
  static const success = 'Success!';
  static const rates = 'Rates';
  static const convert = 'Convert';
  static const ratesLoading = 'Rates loading...';
  static const ratesError = 'Something went wrong...';
  static const from = 'From';
  static const to = 'To';
  static const cancel = 'Cancel';
  static const confirm = 'Confirm';
}
