import 'dart:ui';

class AppColor {
  static const Color grey = Color.fromRGBO(204, 206, 209, 1);

  static const Color darkGrey = Color.fromRGBO(128, 133, 140, 1);

  static const Color lightGrey = Color.fromRGBO(249, 249, 249, 1);
  static const Color blue = Color.fromRGBO(0, 102, 255, 1);
}
