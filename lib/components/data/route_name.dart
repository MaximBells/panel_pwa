import 'package:flutter/material.dart';
import 'package:panel_pwa/blocks/auth/screens/auth_screen.dart';
import 'package:panel_pwa/blocks/home/screens/home_screen.dart';
import 'package:panel_pwa/components/screens/splash_screen.dart';

enum RouteName {
  splash(SplashScreen()),
  home(HomeScreen()),
  auth(AuthScreen());

  final Widget screen;

  const RouteName(this.screen);

  factory RouteName.fromString(String? name) {
    for (var element in RouteName.values) {
      if (element.name == name) {
        return element;
      }
    }
    return RouteName.auth;
  }
}
