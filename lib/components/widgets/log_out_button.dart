import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:panel_pwa/blocks/home/data/home_controller.dart';

class LogOutButton extends StatelessWidget {
  const LogOutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        context.read<HomeController>().logOut();
      },
      icon: const Icon(Icons.logout),
      iconSize: 24,
    );
  }
}
