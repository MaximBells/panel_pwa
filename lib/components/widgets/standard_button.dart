import 'package:flutter/material.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/widgets/margin.dart';

class StandardButton extends StatelessWidget {
  const StandardButton({
    Key? key,
    this.margin,
    required this.onPressed,
    this.text,
    this.textColor = Colors.white,
    this.backGroundColor = AppColor.blue,
  }) : super(key: key);
  final EdgeInsetsGeometry? margin;
  final VoidCallback onPressed;

  final String? text;
  final Color? textColor;
  final Color? backGroundColor;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Margin(
          margin: margin,
          child: ElevatedButton(
            onPressed: onPressed,
            style: ElevatedButton.styleFrom(
                backgroundColor: backGroundColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4))),
            child: Text(
              text ?? "",
              style: TextStyle(
                color: textColor,
                fontWeight: FontWeight.w500,
                fontSize: 14,
              ),
            ),
          )),
    );
  }
}
