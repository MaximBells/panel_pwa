import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/widgets/margin.dart';

class StandardTextField extends StatelessWidget {
  const StandardTextField({
    super.key,
    this.margin,
    this.textEditingController,
    this.inputFormatters,
    this.suffix,
    this.readOnly = false,
    this.onTap,
  });

  final EdgeInsetsGeometry? margin;

  final TextEditingController? textEditingController;
  final List<TextInputFormatter>? inputFormatters;
  final Widget? suffix;
  final bool readOnly;

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return Margin(
      margin: margin,
      child: CupertinoTextField(
        readOnly: readOnly,
        inputFormatters: inputFormatters,
        controller: textEditingController,
        suffix: suffix,
        onTap: onTap,
        padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 8),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: AppColor.grey)),
      ),
    );
  }
}
