import 'package:flutter/material.dart';
import 'package:panel_pwa/components/widgets/margin.dart';

class StandardText extends StatelessWidget {
  const StandardText({
    Key? key,
    required this.text,
    this.margin,
    this.style,
    this.textAlign,
  }) : super(key: key);

  final String text;

  final EdgeInsetsGeometry? margin;

  final TextStyle? style;

  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Margin(
        margin: margin,
        child: Text(
          text,
          style: style,
          textAlign: textAlign,
        ));
  }
}
