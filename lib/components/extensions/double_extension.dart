import 'dart:math';

extension DoubleExtensionNullable on double? {
  double? toDoubleAsFixed(int fractionDigits) {
    return double.tryParse(this?.toStringAsFixed(fractionDigits) ?? "");
  }
}

extension DoubleExtension on double {
  double minimizeDouble({int numbersAfterDot = 2}) {
    if (toString().contains('.') == false) {
      return this;
    }
    final int digitsAfterDot =
        int.parse(toString().substring(toString().indexOf('.') + 1));
    final int parsedDigitsAfterDot =
        (digitsAfterDot / pow(10, digitsAfterDot.toString().length - 2).toInt())
                .floor() *
            pow(10, numbersAfterDot).toInt();
    return double.parse(
        '${toString().substring(0, toString().indexOf('.'))}.$parsedDigitsAfterDot');
  }
}
