import 'package:dio/dio.dart';

enum RequestType {
  get,
  post;

  const RequestType();

  String get type => name.toUpperCase();
}

class RequestService {
  final _dio = Dio();

  Future<dynamic> makeRequest(
    String url, {
    RequestType requestType = RequestType.get,
  }) async {
    try {
      final response = await _dio.request(url,
          options: Options(
            method: requestType.type,
          ));
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      return null;
    }
  }
}
