import 'dart:convert';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart' as enc;

class CryptService {
  final _checkPhrase = '0Shw2fxymCVRhYsU59dq2Q==';

  ///function to create hash with MD5
  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  ///encrypting function
  String encrypt(String text, String password) {
    final key = enc.Key.fromUtf8(generateMd5(password));
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = encrypter.encrypt(text, iv: iv);
    return encrypted.base64;
  }

  ///decrypting function
  String decrypt(String text, String password) {
    final key = enc.Key.fromUtf8(generateMd5(password));
    final iv = enc.IV.fromLength(16);
    final encrypter = enc.Encrypter(enc.AES(key));
    final encrypted = enc.Encrypted.fromBase64(text);
    final content = encrypter.decrypt(encrypted, iv: iv);
    return content;
  }

  bool checkPassword(String password) {
    try {
      decrypt(_checkPhrase, generateMd5(password));
      return true;
    } catch (e) {
      return false;
    }
  }
}
