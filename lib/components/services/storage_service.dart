import 'package:get_storage/get_storage.dart';
import 'package:panel_pwa/components/di.dart';
import 'package:panel_pwa/components/services/crypt_service.dart';

enum StoreKey {
  authAttempt,
}

class StorageService {
  final _storage = GetStorage();

  Future<StorageService> init() async {
    await GetStorage.init();
    return this;
  }

  dynamic getValue(StoreKey key) =>
      _storage.read(getIt<CryptService>().generateMd5(key.name));

  Future<dynamic> writeValue(StoreKey key, dynamic value) async {
    await _storage.write(getIt<CryptService>().generateMd5(key.name), value);
    return value;
  }

  Future<void> clearAll() async {
    await _storage.erase();
  }
}
