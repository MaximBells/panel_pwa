import 'package:flutter/material.dart';
import 'package:panel_pwa/components/di.dart';

class SnackBarService {
  static void showSnackBar(
    String text, {
    Color backGroundColor = Colors.blue,
    Color textColor = Colors.white,
  }) {
    if (materialKey.currentContext == null) {
      return;
    }
    ScaffoldMessenger.of(materialKey.currentContext!).showSnackBar(
      SnackBar(
        content: Text(
          text,
          style: TextStyle(
            color: textColor,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        backgroundColor: backGroundColor,
        showCloseIcon: true,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        behavior: SnackBarBehavior.floating,
      ),
    );
  }
}
