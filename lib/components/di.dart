import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:panel_pwa/components/services/crypt_service.dart';
import 'package:panel_pwa/components/services/request_service.dart';
import 'package:panel_pwa/components/services/storage_service.dart';

final getIt = GetIt.instance;

final GlobalKey<NavigatorState> materialKey = GlobalKey<NavigatorState>();

Future<void> initServices() async {
  getIt.registerSingleton(CryptService());
  getIt.registerSingleton(await StorageService().init());
  getIt.registerSingleton(RequestService());
}
