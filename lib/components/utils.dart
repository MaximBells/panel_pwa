import 'package:flutter/cupertino.dart';
import 'package:panel_pwa/blocks/convert/data/convert_cubit.dart';
import 'package:panel_pwa/components/static/app_color.dart';
import 'package:panel_pwa/components/static/app_string.dart';

class Utils {
  static void showDialog(
    BuildContext context, {
    int initialTerm = 0,
    required Function(int) onSelected,
    required List<Widget> children,
  }) {
    int chosenIndex = 0;
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => Container(
        height: MediaQuery.of(context).size.height * 0.35,
        padding: const EdgeInsets.only(top: 6.0),
        // The Bottom margin is provided to align the popup above the system navigation bar.
        margin: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        // Provide a background color for the popup.
        color: CupertinoColors.systemBackground.resolveFrom(context),
        // Use a SafeArea widget to avoid system overlaps.
        child: SafeArea(
          top: false,
          child: Stack(
            children: [
              CupertinoPicker(
                looping: true,
                magnification: 1.22,
                squeeze: 1.2,
                useMagnifier: true,
                itemExtent: 32,
                // This sets the initial item.
                scrollController: FixedExtentScrollController(
                  initialItem: initialTerm,
                ),
                // This is called when selected item is changed.
                onSelectedItemChanged: (item) {
                  chosenIndex = item;
                },
                children: children
                    .asMap()
                    .entries
                    .map((e) => GestureDetector(
                          onTap: () {
                            onSelected(e.key);
                            Navigator.pop(context);
                          },
                          child: e.value,
                        ))
                    .toList(),
              ),
              Align(
                  alignment: Alignment.topCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CupertinoButton(
                          child: const Text(
                            AppString.cancel,
                            style: TextStyle(color: AppColor.blue),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      CupertinoButton(
                          child: const Text(
                            AppString.confirm,
                            style: TextStyle(color: AppColor.blue),
                          ),
                          onPressed: () {
                            onSelected(chosenIndex);
                            Navigator.pop(context);
                          }),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
