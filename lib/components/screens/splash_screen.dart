import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:panel_pwa/components/data/route_name.dart';
import 'package:panel_pwa/components/di.dart';
import 'package:panel_pwa/components/services/snackbar_service.dart';
import 'package:panel_pwa/components/services/storage_service.dart';
import 'package:panel_pwa/components/static/app_string.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  DateTime get _lastAttempt => DateTime.fromMillisecondsSinceEpoch(
      getIt<StorageService>().getValue(StoreKey.authAttempt));

  void _init() async {
    await initServices();
    await Future.delayed(const Duration(seconds: 2));
    if (getIt<StorageService>().getValue(StoreKey.authAttempt) != null &&
        DateTime.now().difference(_lastAttempt).inHours <= 24) {
      getIt<StorageService>().writeValue(
          StoreKey.authAttempt, DateTime.now().millisecondsSinceEpoch);
      materialKey.currentState?.pushReplacementNamed(RouteName.home.name);
    } else {
      materialKey.currentState?.pushReplacementNamed(RouteName.auth.name);
      SnackBarService.showSnackBar(AppString.needToLogIn,
          backGroundColor: Colors.red);
    }
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
        body: Center(
      child: CupertinoActivityIndicator(
        color: Colors.blue,
        radius: 16,
      ),
    ));
  }
}
