# Финансовое приложение

## Как запустить:

Для того, чтобы запустить приложение, нужно склонировать к себе гит репозиторий, а потом запустить на IOS или Android


## Используемый стек технологий:

- CUBIT (bloc) для стейт менеджемента
- GetIt для DI
- DIO для http запросов
- GetStorage для локального хранения данных
- Encrypt и Crypto для шифрования

## Используемое шифрования в приложении

- Используемый алгоритм шифрования: AES
- Используемый алгоритм хэширования: MD5
